CREATE DATABASE SpringEhCacheExample;

CREATE TABLE SpringEhCacheExample.`customer` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `AGE` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP USER 'spring'@'localhost';

CREATE USER 'spring'@'localhost' IDENTIFIED BY 'test';

GRANT ALL PRIVILEGES ON * . * TO 'spring'@'localhost';

FLUSH PRIVILEGES;
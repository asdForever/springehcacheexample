package kz.cache.test;

import kz.cache.test.model.Customer;
import kz.cache.test.repository.CustomerRepository;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Sorokin Andrey
 * @since 03.11.2017.
 */
public class Application {

    private static final Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {

        final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        final CustomerRepository customerRepository = (CustomerRepository) context.getBean("customerRepository");
        final Customer customer = new Customer("Test", 18);

        final int customerId = customerRepository.insert(customer);

        getCache(customerRepository, customerId);

        customer.setId(customerId);
        customer.setName("New Name");
        customerRepository.update(customer);

        getCache(customerRepository, customerId);
    }

    private static void getCache(final CustomerRepository customerRepository, final int customerId) {

        final long startTime = System.currentTimeMillis();

        for (int i = 0; i < 10; i++) {
            final Customer customer = customerRepository.findByCustomerId(customerId);
            logger.info(customer.toString());
            slowQuery(1);
        }
        logger.info("getCache result execution time in milliseconds: " + String.valueOf(System.currentTimeMillis() - startTime));
    }

    private static void slowQuery(long seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
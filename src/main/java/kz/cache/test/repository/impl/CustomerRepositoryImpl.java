package kz.cache.test.repository.impl;

import kz.cache.test.model.Customer;
import kz.cache.test.repository.CustomerRepository;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;

import javax.sql.DataSource;
import java.sql.*;

/**
 * @author Sorokin Andrey
 * @since 06.11.2017.
 */
public class CustomerRepositoryImpl implements CustomerRepository {

    private static final Logger logger = Logger.getLogger(CustomerRepositoryImpl.class);

    private DataSource dataSource;

    public int insert(Customer customer) {
        logger.info("inserting new customer");

        int customerId = 0;
        try {
            try (Connection connection = dataSource.getConnection()) {
                final String sql = "INSERT INTO CUSTOMER (NAME, AGE) VALUES (?, ?) ON DUPLICATE KEY UPDATE NAME=?, AGE=?";
                final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, customer.getName());
                ps.setInt(2, customer.getAge());
                ps.setString(3, customer.getName());
                ps.setInt(4, customer.getAge());
                ps.executeUpdate();

                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    customerId = rs.getInt(1);
                }

                ps.close();

                return customerId;
            }
        } catch (SQLException e) {
            logger.error("Error during execution insertion for customer with id = " + customer.getId() + ", " + e);
        }

        return 0;
    }

    public void update(Customer customer) {
        logger.info("updating customer with id = " + customer.getId());

        try {
            try (Connection connection = dataSource.getConnection()) {
                final String sql = "UPDATE CUSTOMER SET NAME = ?, AGE = ? WHERE ID = ?";
                final PreparedStatement ps = connection.prepareStatement(sql);

                ps.setString(1, customer.getName());
                ps.setInt(2, customer.getAge());
                ps.setInt(3, customer.getId());
                ps.executeUpdate();

                ps.close();
            }
        } catch (SQLException e) {
            logger.error("Error during execution updating customer with id = " + customer.getId() + ", " + e);
        }
    }

    @Cacheable(value="sampleCache3", key="#id")
    public Customer findByCustomerId(int id) {
        try {
            try (Connection con = dataSource.getConnection()) {
                try (PreparedStatement ps = createGetStatement(con, id)) {
                    final ResultSet rs = ps.executeQuery();
                    Customer customer = null;

                    if (rs.next()) {
                        customer = new Customer(
                                rs.getInt("ID"),
                                rs.getString("NAME"),
                                rs.getInt("Age")
                        );
                    }

                    return customer;
                }
            }
        } catch (SQLException e) {
            logger.error("Error during execution findByCustomerId for customer with id = " + id + ", " + e);
        }
        return null;
    }

    private PreparedStatement createGetStatement(final Connection connection, final int id) throws SQLException {
        final String sql = "SELECT * FROM CUSTOMER WHERE ID = ?";
        final PreparedStatement ps = connection.prepareStatement(sql);

        ps.setInt(1, id);

        return ps;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}

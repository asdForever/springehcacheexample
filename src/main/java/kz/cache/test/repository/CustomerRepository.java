package kz.cache.test.repository;

import kz.cache.test.model.Customer;

/**
 * @author Sorokin Andrey
 * @since 06.11.2017.
 */
public interface CustomerRepository {
    int insert(Customer customer);
    void update(Customer customer);
    Customer findByCustomerId(int id);
}